var knight = {
    // properti
    type: "Chess",
    move: [],

    // exemple the position in koordinat 0 of [x & Y]
    // method
    movement1: function () {
        return this.move[2, 1]
    },
    movement2: function () {
        return this.move[1, 2]
    },
    movement3: function () {
        return this.move[-1, 2]
    },
    movement4: function () {
        return this.move[-2, 1]
    },
    movement5: function () {
        return this.move[-2, -1]
    },
    movement6: function () {
        return this.move[-1, -2]
    },
    movement7: function () {
        return this.move[1, -2]
    },
    movement8: function () {
        return this.move[2, -1]
    }
}

knight.movement1()
knight.movement2()
knight.movement3()
knight.movement4()
knight.movement5()
knight.movement6()
knight.movement7()
knight.movement8()
